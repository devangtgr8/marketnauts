class SessionsController < ApplicationController
  def create
    auth = request.env['omniauth.auth']
    authcode = auth[:provider] + ";" + auth[:uid]

    @user = User.find_or_create_by_authcode(authcode)
    @user.uid = auth[:uid]
    @user.name = auth[:info][:name]
    @user.email = auth[:info][:email]
    @user.save
    session[:user_id] = @user.id

    flash[:success] = "Successfully logged in."
    redirect_to "/"
  end

  def failure
    flash[:error] = "Something went wrong."
    redirect_to "/"
  end

  def destroy
    session[:user_id] = nil

    flash[:success] = "Logged out."
    redirect_to "/"
  end
end
