class StocksController < ApplicationController
  before_filter :require_logged_in!

  def index
    @stocks = Stock.all
    render :empty if @stocks.length == 0
  end

  def show
    @stock = Stock.find_by_name( params[:id] )
    respond_to do |format|
      format.json { render :json => @stock.prices.to_json }
    end
  end
end
