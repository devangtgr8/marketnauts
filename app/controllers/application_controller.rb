class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user, :logged_in?

  def logged_in?
    not session[:user_id].nil?
  end

  def current_user
    if logged_in?
      User.find_by_id(session[:user_id])
    else
      false
    end
  end

  def require_logged_in!
    if not logged_in?
      flash[:info] = "Please sign in to proceed."
      redirect_to "/"
    end
  end
end
