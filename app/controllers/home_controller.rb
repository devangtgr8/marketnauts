class HomeController < ApplicationController
  def index
    if logged_in?
      @leaderboard = Leaderboard.top(10)
      @user = current_user
      @rank         = @user.rank
      @cash_on_hand = @user.cash_on_hand
      @portfolio    = @user.portfolio_value
      @stocks       = @user.stocks
      @stocks_owned = @user.stocks.any? {|k, v| v > 0 }
      render :dashboard
    else
      render :index
    end
  end

  def leaderboard
    @leaderboard = Leaderboard.all.sort_by &:rank
  end
end
