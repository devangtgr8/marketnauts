class Leaderboard < ActiveRecord::Base
  attr_accessible :rank, :user_id
  belongs_to :user

  def self.recompute!
    users = User.all.sort_by {|x| x.portfolio_value }.reverse
    users.each_with_index do |user, i|
      lb = Leaderboard.find_or_create_by_rank(i+1)
      lb.user = user
      lb.save
    end
    Leaderboard.all.select {|x| x.rank > users.length }.each {|x| x.delete }
  end

  def self.top(n)
    lb = Leaderboard.all.sort_by {|x| x.rank }
    lb = lb[0..(n-1)] if lb.length > n
    lb
  end
end
