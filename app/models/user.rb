class User < ActiveRecord::Base
  attr_accessible :authcode, :email, :name, :uid, :cash_on_hand, :stocks
  has_one :leaderboard
  serialize :stocks, Hash

  # Require that authcode, name and uid be present.
  validates :authcode, :name, :uid, :presence => true

  # For now, return the cash on hand.
  def portfolio_value
    cash_on_hand
  end

  def rank
    leaderboard.rank
  end
end
