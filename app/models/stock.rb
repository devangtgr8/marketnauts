class Stock < ActiveRecord::Base
  attr_accessible :name, :prices, :futures

  serialize :prices, Array
  serialize :futures, Array # deltas

  validates :name, :presence => true, :uniqueness => true

  def identifier
    name.gsub(/\s/, '')
  end

  # How much history to display at all times.
  def max_history
    500
  end

  def step
    prices.shift if prices.length >= max_history

    while prices.length < max_history
      futures.push 10*(rand-0.5)
      next_price = prices.last + futures.shift
      prices.push next_price
      prices.pop if prices.last < 0.1
    end

    save
    self
  end

  def clear
    futures.clear
    prices.clear
    save
    self
  end
end
