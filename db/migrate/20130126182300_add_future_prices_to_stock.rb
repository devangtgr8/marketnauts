class AddFuturePricesToStock < ActiveRecord::Migration
  def change
    add_column :stocks, :futures, :text
  end
end
