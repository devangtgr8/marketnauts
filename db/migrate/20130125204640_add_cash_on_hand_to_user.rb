class AddCashOnHandToUser < ActiveRecord::Migration
  def change
    add_column :users, :cash_on_hand, :decimal, :default => 100_000
  end
end
