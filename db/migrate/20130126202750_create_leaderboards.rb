class CreateLeaderboards < ActiveRecord::Migration
  def change
    create_table :leaderboards, :id => false do |t|
      t.primary_key :rank
      t.integer :user_id

      t.timestamps
    end
  end
end
