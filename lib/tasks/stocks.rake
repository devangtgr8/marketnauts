namespace :stocks do
  desc "Delete all stocks."
  task :clear => :environment do
    Stock.all.each {|x| x.delete }
  end

  desc "Add a new stock."
  task :add, [:name, :price] => :environment do |t, args|
    s = Stock.new
    s.name = args[:name]
    s.prices.push args[:price].to_i
    s.save
  end

  desc "Move all stocks forward by a tick."
  task :step => :environment do
    Stock.all.each {|x| x.step }
  end
end
