namespace :leaderboard do
  desc "Recompute the leaderboard."
  task :recompute => :environment do
    Leaderboard.recompute!
  end
end
