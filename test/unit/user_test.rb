require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "can't create user without authcode" do
    u = User.new
    u.name = "asdf"
    u.uid = "asdf"
    assert !u.save
  end

  test "can't create user without name" do
    u = User.new
    u.authcode = "asdf"
    u.uid = "asdf"
    assert !u.save
  end

  test "can't create user without uid" do
    u = User.new
    u.authcode = "asdf"
    u.name = "asdf"
    assert !u.save
  end

  test "default cash on hand is 100000" do
    u = User.new
    u.authcode = "asdf"
    u.name = "asdf"
    u.uid = "asdf"
    assert( u.save )
    assert( u.cash_on_hand == 100000 )
  end
  # end
end
