require 'test_helper'

class StockTest < ActiveSupport::TestCase
  test "can't create a stock without a name" do
    s = Stock.new
    assert !s.save
  end

  test "stock name must be unique" do
    s = Stock.new
    s.name = "AAPL"
    assert s.save
    s = Stock.new
    s.name = "AAPL"
    assert !s.save
  end

  test "can clear prices" do
    s = Stock.new
    s.name = "Apple"
    s.prices.push 20
    assert s.save
    s.step
    s.clear
    assert_equal s.prices.length, 0
  end

  test "step fills out the history" do
    s = Stock.new
    s.name = "Apple"
    s.prices.push 20
    assert s.save
    s.step
    assert_equal s.prices.length, s.max_history
  end
end
