require 'test_helper'

class LeaderboardTest < ActiveSupport::TestCase
  test "can recompute and display top n correctly" do
    u1 = User.new
    u1.name = u1.authcode = u1.uid = "1"
    u1.cash_on_hand = 2
    assert u1.save
    u2 = User.new
    u2.name = u2.authcode = u2.uid = "2"
    u2.cash_on_hand = 1
    assert u2.save
    Leaderboard.recompute!
    assert_equal Leaderboard.top(1)[0].user, 
                 User.all.sort_by {|x| -x.portfolio_value }[0]
    assert_equal Leaderboard.top(User.count+1)[1].user, 
                 User.all.sort_by {|x| -x.portfolio_value }[1]
  end
end
